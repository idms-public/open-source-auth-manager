FROM ruby:2.5.1

### System Libraries
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y zip unzip



### Install Oracle Instant Client ###
RUN apt-get install libaio1
RUN mkdir -p /opt/oracle

WORKDIR /opt/oracle
ADD authentication_manager/vendor/oracle/instantclient-basic-linux.x64-12.1.0.2.0.zip /opt/oracle/instantclient-basic-linux.x64-12.1.0.2.0.zip
ADD authentication_manager/vendor/oracle/instantclient-sdk-linux.x64-12.1.0.1.0.zip /opt/oracle/instantclient-sdk-linux.x64-12.1.0.1.0.zip
ADD authentication_manager/vendor/oracle/instantclient-sqlplus-linux.x64-12.1.0.1.0.zip /opt/oracle/instantclient-sqlplus-linux.x64-12.1.0.1.0.zip

RUN unzip instantclient-basic-linux.x64-12.1.0.2.0.zip
RUN unzip instantclient-sdk-linux.x64-12.1.0.1.0.zip
RUN unzip instantclient-sqlplus-linux.x64-12.1.0.1.0.zip

RUN cd /opt/oracle/instantclient_12_1; ln -s libclntsh.so.12.1 libclntsh.so
ENV LD_LIBRARY_PATH="/opt/oracle/instantclient_12_1"


WORKDIR /rails-app
ADD authentication_manager/Gemfile.lock .
ADD authentication_manager/Gemfile .
RUN gem install bundler && bundle install

ADD authentication_manager ./

EXPOSE 3000
EXPOSE 9090
