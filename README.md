# Authentication Manager
## Table of Contents
1. [Useful Commands](#useful-commands)
2. [Authentication Manager Docs](#auth-manager)
  * [Docker Setup](#docker-setup)
  * [Database Setup](#database)
  * [Application Configuration](#app-setup)
3. [SPREG Docs](#spref)
4. [Troubleshooting](#troubleshoot)


## Useful Commands
note(see initial configuration if this is a fresh install)
- `docker ps -a` show all running docker containers
- `docker-compose up -d` create docker containers based off of a `docker-compose.yml` the `-d` flag runs containers in a detached state
- `docker-compose down` destroy docker containers based off of a `docker-compose.yml`
- `docker-compose stop` stop docker containers based off of a `docker-compose.yml`
- `docker-compose start` start docker containers based off of a `docker-compose.yml`

**note** you typically don't want to destroy containers, rather stop and start them as needed

- `docker-compose exec auth-manager rails c` opens up a rails console
- `docker-compose exec auth-manager rake` runs the test suite
- `docker-compose exec auth-manager bash` opens a bash console on the webapp container
- `docker attach auth-manager` enables you to see console output from the container

# Authentication Manager Documentation <a name="auth-manager"></a>
## Setup
## 1. Build docker images and start containers. <a name="docker-setup"></a>
  **NOTE: run all commands from root of repository**
  - if not already installed be install both `docker` and `docker-compose`  https://docs.docker.com/engine/installation/ and https://docs.docker.com/compose/install/
  - clone down the repo with submodules `git clone --recurse-submodules` git@idms-git.oit.duke.edu:idms/open-source-auth-manager.git https://github.com/chaconinc/MainProject`
  - run `docker-compose build` This will create 5 docker images
    - auth-manager (web ui, localhost:3000)
    - mariadb (db for dev)
    - adminer (db admin web ui for localhost:8080)
    - spreg (backend)
  - run `docker-compose build` you will only need to do this the first time and anytime you modify the Gemfile
  - run `docker-compose up -d; docker-compose logs -f` this will spin up a container for authentication manager on localhost:3000.
  - run `docker ps -a` and you should see the 4 containers running

## 2. Configure Database <a name="database"></a>
  - run `docker-compose exec auth-manager bash` This will create a bash session inside the auth-manager container.
  - Inside the bash session run the following commands in order:
    - `rails db:migrate` - migrates tables to db.
    - `rails db:seed` - sets up db with mock values, this can be skipped if desired.
  - the spreg process needs to be started since it needs the db to run and it probably failed.
    - `docker-compose exec spreg bash`
    - `cd /src/idms/applications/spreg/bin`
    - `./start.sh`

## 3. Configure Application <a name="open-app"></a>
### 1. Secrets
The `authentication_manager/config/secrets.yml` file holds a lot of the configurable values for this application. Note that "out of the box" the app is configured to allow you to explore the functionality with minimal configuration. If you were to use this app in some sort of production environement you would need to modify certain values. Here is a rundown for each value:

  * **secret_key_base:** this is a secret key rails apps uses to protect against CSRF. This value should be read from an ENV variable named AUTH_MAN_SECRET_KEY_BASE in production.
  * **use_mattermost:** the app supports alerts to mattermost for integration requests and attribute approval alerts.
  * **mattermost_webhook_url:** mattermost webhook url if using mattermost
  * **grouper_admin_source:** the app can use grouper groups for authorization. Set to true if you plan to use grouper as an admin source
  * **grouper_url:** base url for your grouper instance
  * **grouper_membership_path:** path to grouper group membership api
  * **grouper_admin_group:** urn path of group you consider to be the admins of this webapp
  * **grouper_user:** grouper api user, reads from ENV var AUTH_MAN_GROUPER_USER
  * **grouper_pass:** grouper api password, reads from ENV var AUTH_MAN_GROUPER_PASS
  * **NOTE: the following ldap values are only meaningful if using grouper**
  * **ldap_host:** ldap host
  * **ldap_port:** ldap port
  * **ldap_base:** ldap base
  * **ldap_timeout:** ldap timeout
  * **ldap_encoding:** ldap encoding
  * **use_auth_secrets:** this is Duke specific and should always be set to false
  * **legacy_db:** this is Duke specific and should always be set to false
  * **logout_url:** url to log user out of SP session
  * **auth_type:** type of authentication you wish to use in the environment. You have 3 options:
    * *mock* - simply mocks shib headers but no actual use of an IDP or SP. Useful when exploing the app in a dev environment
    * *samlCamel* - a gem that operates as a native sp at the application level. Useful when wanting to use a true SAML expierence in the dev environment.
    * *shib* - reads attributes from a Shibboleth SP installed in front of the applications

### 2. Settings Files
Configurable settings files live in `authentication_manager/config/settings`
  * `admins.json` - if not using grouper this is where you define who yor admins are. You need to supply uid and displayName.
  * `grouper_admin_groups.json` - if on the other hand you are using grouper for admins you need to define the urn path of the admin groups within the groups array of the json object.
  * `mock_ids.json` - used in development to mock the identity of other users, you need to define the key: value pairs of each attribute you would like to define. Those attributes will be mocked as if released by the shib IDP and consumed accordingly

### 5. shibboleth xml files
- a svn respository is set up in the spreg container. 
  - `mkdir -p shibboleth_idp\metadata`
  - `mkdir -p shibboleth_idp\conf`
  - Place XML files in correct dirs
    - local-sites.xml into metadata
    - attribute-filter.xml attribute-resolver.xml relying-party.xml into conf

## Troubleshooting <a name="troubleshoot"></a>
git
  * getting the submodules
    * `git submodule init`
    * `git submodule update`


## Development
1. cloning the repository `git clone --recurse-submodules git@idms-git.oit.duke.edu:idms/open-source-auth-manager.git`
2. Making commits
  - this project uses git submodules. The root of the repo is the main git repo and works as normal
  - the `authentication_manager` and `spreg` directories are submodules so things work slightly different there
    - when working in a submodule run `git log` to see where the head is at. You're may be checked out to a commit after a
      clone by default, what you want to do now is checkout the branch of that commit. So if your commit is ```commit 9888415eef6d44e8c275d45d1a7bd44830b1f9b4 (HEAD -> open-source, origin/open-source)````
      run `git checkout open-source`
    - once you are in a regular branch you can work in git as normal.
    - when you push and commit a submodule you must also do so for the same for the main repo. For example if you make a commit and push a change in spreg, cd into the root of the project and make a new commit. This will make note of what commit hash spreg is currently on.
  - when you need to pull changes, run `git submodule update --recursive --remote` from the root of the repo



## Comments  <a name="comments"></a>
- Does the docker attach need a container name, auth-manager
- Setting up the ssh/known hosts files might be possible running ssh-keyscan > known_host
